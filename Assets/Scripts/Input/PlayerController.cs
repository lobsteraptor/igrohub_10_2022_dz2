using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody playerRigidbody;
    private Vector3 startPositions;

    [SerializeField] private float moveSpeed = 600f;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        playerRigidbody = GetComponent<Rigidbody>();
        startPositions = transform.position;
        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();
        playerInputActions.Kart.Boost.performed += context => BoostKart();
        EnhancedTouchSupport.Enable();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 moveDirections = playerInputActions.Kart.Move.ReadValue<Vector2>();
        Vector2 moveDirectionsJoystick = playerInputActions.Kart.MoveJoystick.ReadValue<Vector2>();
        if (moveDirections.x == 0 && moveDirections.y == 0)
            Move(moveDirectionsJoystick);
        else
            Move(moveDirections);
    }

    private void Move(Vector2 direction)
    {
        playerRigidbody.velocity = new Vector3(direction.x * moveSpeed * Time.fixedDeltaTime, 0f, direction.y * moveSpeed * Time.fixedDeltaTime);
    }

    private void ResetPosition()
    {
        playerRigidbody.MovePosition(startPositions);
        playerRigidbody.MoveRotation(Quaternion.identity);
    }

    private void BoostKart()
    {
        moveSpeed = 1200f;
    }
}
