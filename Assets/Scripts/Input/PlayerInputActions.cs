//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.3.0
//     from Assets/Scripts/Input/PlayerInputActions.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @PlayerInputActions : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Kart"",
            ""id"": ""ba4f908a-eb50-4475-b1c3-96adb0eaf53b"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""f627e0f3-a15c-42db-a7ac-77fe5612d41c"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""ResetPosition"",
                    ""type"": ""Button"",
                    ""id"": ""9f96e6dc-04d4-4497-a651-c9521597632a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Boost"",
                    ""type"": ""Button"",
                    ""id"": ""4ad700be-4f81-4aa2-b7e1-4ddb07a384d6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MoveJoystick"",
                    ""type"": ""Value"",
                    ""id"": ""69057d5c-276c-4d45-9414-b7141868cfe6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""LookJoystick"",
                    ""type"": ""Value"",
                    ""id"": ""10c2a433-7412-48ef-bb3a-d2c39355fab5"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""2726dc98-fe22-44c9-9fa9-e256648ebcc6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""ca539a00-a8e8-4c2b-84d6-1380015cee5d"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""5ad3658f-d8bf-45f0-b090-ed2b41c2d871"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""8fca4a5c-7f89-49fa-84b9-ace9ec370623"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""8fbe22a7-5141-4b5d-988e-dead1c81c509"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""ad9f20bc-8247-4ae8-a51c-7bb1385378e7"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""1fc13fac-3a23-4cce-8409-192a492ca9e5"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""25497701-a489-433c-8f3b-cb3c525268d5"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""d00cd557-9208-4725-9d6d-474e58791b50"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a12ad8b3-9463-45ee-b1ca-1aa25d835b5d"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""38eca6c8-9536-497c-81a2-68cd6d5bec5d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""ResetPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""036cd9b5-6c2f-494b-81b3-6bb6bd25470c"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Boost"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2edeee0f-db7f-463a-8e1c-79ea205f9f9f"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveJoystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3892acb9-1309-4b4c-8740-8c49054c95d5"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""LookJoystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PC"",
            ""bindingGroup"": ""PC"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Kart
        m_Kart = asset.FindActionMap("Kart", throwIfNotFound: true);
        m_Kart_Move = m_Kart.FindAction("Move", throwIfNotFound: true);
        m_Kart_ResetPosition = m_Kart.FindAction("ResetPosition", throwIfNotFound: true);
        m_Kart_Boost = m_Kart.FindAction("Boost", throwIfNotFound: true);
        m_Kart_MoveJoystick = m_Kart.FindAction("MoveJoystick", throwIfNotFound: true);
        m_Kart_LookJoystick = m_Kart.FindAction("LookJoystick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // Kart
    private readonly InputActionMap m_Kart;
    private IKartActions m_KartActionsCallbackInterface;
    private readonly InputAction m_Kart_Move;
    private readonly InputAction m_Kart_ResetPosition;
    private readonly InputAction m_Kart_Boost;
    private readonly InputAction m_Kart_MoveJoystick;
    private readonly InputAction m_Kart_LookJoystick;
    public struct KartActions
    {
        private @PlayerInputActions m_Wrapper;
        public KartActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Kart_Move;
        public InputAction @ResetPosition => m_Wrapper.m_Kart_ResetPosition;
        public InputAction @Boost => m_Wrapper.m_Kart_Boost;
        public InputAction @MoveJoystick => m_Wrapper.m_Kart_MoveJoystick;
        public InputAction @LookJoystick => m_Wrapper.m_Kart_LookJoystick;
        public InputActionMap Get() { return m_Wrapper.m_Kart; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(KartActions set) { return set.Get(); }
        public void SetCallbacks(IKartActions instance)
        {
            if (m_Wrapper.m_KartActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_KartActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnMove;
                @ResetPosition.started -= m_Wrapper.m_KartActionsCallbackInterface.OnResetPosition;
                @ResetPosition.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnResetPosition;
                @ResetPosition.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnResetPosition;
                @Boost.started -= m_Wrapper.m_KartActionsCallbackInterface.OnBoost;
                @Boost.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnBoost;
                @Boost.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnBoost;
                @MoveJoystick.started -= m_Wrapper.m_KartActionsCallbackInterface.OnMoveJoystick;
                @MoveJoystick.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnMoveJoystick;
                @MoveJoystick.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnMoveJoystick;
                @LookJoystick.started -= m_Wrapper.m_KartActionsCallbackInterface.OnLookJoystick;
                @LookJoystick.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnLookJoystick;
                @LookJoystick.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnLookJoystick;
            }
            m_Wrapper.m_KartActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @ResetPosition.started += instance.OnResetPosition;
                @ResetPosition.performed += instance.OnResetPosition;
                @ResetPosition.canceled += instance.OnResetPosition;
                @Boost.started += instance.OnBoost;
                @Boost.performed += instance.OnBoost;
                @Boost.canceled += instance.OnBoost;
                @MoveJoystick.started += instance.OnMoveJoystick;
                @MoveJoystick.performed += instance.OnMoveJoystick;
                @MoveJoystick.canceled += instance.OnMoveJoystick;
                @LookJoystick.started += instance.OnLookJoystick;
                @LookJoystick.performed += instance.OnLookJoystick;
                @LookJoystick.canceled += instance.OnLookJoystick;
            }
        }
    }
    public KartActions @Kart => new KartActions(this);
    private int m_PCSchemeIndex = -1;
    public InputControlScheme PCScheme
    {
        get
        {
            if (m_PCSchemeIndex == -1) m_PCSchemeIndex = asset.FindControlSchemeIndex("PC");
            return asset.controlSchemes[m_PCSchemeIndex];
        }
    }
    public interface IKartActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnResetPosition(InputAction.CallbackContext context);
        void OnBoost(InputAction.CallbackContext context);
        void OnMoveJoystick(InputAction.CallbackContext context);
        void OnLookJoystick(InputAction.CallbackContext context);
    }
}
